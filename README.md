# Learning Flask and Python

Taken from the following blog post:
https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world

## To enable the virtual environment:

~~~bash
source venv/bin/activate
~~~

## To execute:
~~~bash
export FLASK_APP=microblog.py
flask run
~~~

## To create the database migrations repository for SQLAlchemy
~~~bash
export FLASK_APP=microblog.py    
flask db init
flask db migrate -m "users table"
flask db upgrade
~~~

# To use the DB context from the Python interpreter 
- From your virtual environment
~~~bash
flask shell
~~~